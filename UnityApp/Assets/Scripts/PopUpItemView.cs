﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUpItemView : MonoBehaviour
{
    [SerializeField] private Text _titleText = default;
    [SerializeField] private Text _desciptionText = default;
    [SerializeField] private Image _itemImage = default;

    public void SetInfo(string title, string description)
    {
        _titleText.text = title;
        _desciptionText.text = description;
    }

    public void SetInfo(Texture texture)
    {
        _itemImage.sprite = texture.Tosprite();
    }

    public void ClosePopUp()
    {
        gameObject.SetActive(false);
    }
}
