using System;
using UnityEngine;
using UnityEngine.Assertions;

public class PostOutlineCamera : MonoBehaviour
{
    public Camera Camera { get; private set; }

    public Action<RenderTexture, RenderTexture> Callback;

    private void Awake()
    {
        Camera = GetComponent<Camera>();
        Assert.IsNotNull(Camera);
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Callback?.Invoke(source, destination);
    }
}