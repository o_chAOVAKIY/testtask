﻿Shader "PostOutline"
{
    Properties
    {
        _MainTex("Main Texture",2D)="black"{}
        _SceneTex("Scene Texture",2D)="black"{}
    }
    SubShader 
    {
        Pass 
        {
            CGPROGRAM
     
            sampler2D _MainTex;
            sampler2D _SceneTex;
 
            //<SamplerName>_TexelSize is a float2 that says how much screen space a texel occupies.
            float2 _MainTex_TexelSize;
 
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
             
            struct v2f 
            {
                float4 pos : SV_POSITION;
                float2 uvs : TEXCOORD0;
            };
             
            v2f vert (appdata_base v) 
            {
                v2f o;
                 
                //Despite the fact that we are only drawing a quad to the screen, Unity requires us to multiply vertices by our MVP matrix, presumably to keep things working when inexperienced people try copying code from other shaders.
                o.pos = UnityObjectToClipPos(v.vertex);
                 
                //Also, we need to fix the UVs to match our screen space coordinates. There is a Unity define for this that should normally be used.
                //o.uvs = o.pos.xy * 0.5 + 0.5;
                 o.uvs = ComputeScreenPos(o.pos);
                 
                return o;
            }
             
            half4 frag(v2f i) : COLOR 
            {

                //if something already exists underneath the fragment (in the original texture), discard the fragment.
                if(tex2D(_MainTex,i.uvs.xy).r>0)
                {
                    return tex2D(_SceneTex, float2(i.uvs.x, i.uvs.y));
                }
                
                //arbitrary number of iterations for now
                int NumberOfIterations=9;
 
                //split texel size into smaller words
                float TX_x=_MainTex_TexelSize.x;
                float TX_y=_MainTex_TexelSize.y;
 
                //and a final intensity that increments based on surrounding intensities.
                float ColorIntensityInRadius=0.0;

                float u = 0.0;
                float uu = -(NumberOfIterations >> 1) * TX_x;
                float v = 0.0;
                float vv = 0.0;
                //for every iteration we need to do horizontally
                [unroll]
                for(int k = 0; k < NumberOfIterations; k += 1)
                {
                    //u = i.uvs.x + (k - (NumberOfIterations >> 1)) * TX_x;
                    u = i.uvs.x + uu;
                    vv = -(NumberOfIterations >> 1) * TX_x;
                    //for every iteration we need to do vertically
                    [unroll]
                    for(int j = 0; j < NumberOfIterations; j += 1)
                    {
                        v = i.uvs.y + vv;
                        //increase our output color by the pixels in the area
                        ColorIntensityInRadius += tex2D(_MainTex, float2(u, v)).r;
                        vv += TX_y;
                    }
                    uu += TX_x;
                }
                
                if (ColorIntensityInRadius > 0){
                    return ColorIntensityInRadius * half4(0,1,1,1);
                }

                return tex2D(_SceneTex, i.uvs.xy);
                
            }
             
            ENDCG
        }
    }
}
