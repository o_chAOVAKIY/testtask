﻿using UnityEngine;
using UnityEngine.Assertions;

public class PostOutline : MonoBehaviour
{
    [SerializeField] private Shader _postOutlineShader = default;
    [SerializeField] private Shader _drawSimpleShader = default;

    private PostOutlineCamera _targetCamera;

    private Camera _tmpCamera;

    private Material _postOutlineMaterial;
    private int _layerMask;

    private RenderTexture _tmpRenderTexture;
    private static readonly int SceneTex = Shader.PropertyToID("_SceneTex");

    public void Init(Camera targetCamera)
    {
        Assert.IsNotNull(targetCamera);
        Assert.IsNull(_targetCamera);

        _targetCamera = targetCamera.gameObject.AddComponent<PostOutlineCamera>();
        _targetCamera.Callback = OnApplyOutline;
    }

    private void Awake()
    {
        Init(Camera.main);
        _layerMask = 1 << LayerMask.NameToLayer("Outline");

        _tmpCamera = new GameObject("PostOutline").AddComponent<Camera>();
        _tmpCamera.enabled = false;

        _postOutlineMaterial = new Material(_postOutlineShader);
    }

    private void OnApplyOutline(RenderTexture source, RenderTexture destination)
    {
        //set up a temporary camera
        _tmpCamera.CopyFrom(_targetCamera.Camera);
        _tmpCamera.clearFlags = CameraClearFlags.Color;
        _tmpCamera.backgroundColor = Color.black;

        //cull any layer that isn't the outline
        _tmpCamera.cullingMask = _layerMask;

        if (_tmpRenderTexture == null)
        {
            _tmpRenderTexture = new RenderTexture(source.width, source.height, 0, RenderTextureFormat.Default);

            //put it to video memory
            _tmpRenderTexture.Create();
        }

        //set the camera's target texture when rendering
        _tmpCamera.targetTexture = _tmpRenderTexture;

        //render all objects this camera can render, but with our custom shader.
        _postOutlineMaterial.SetTexture(SceneTex, source);
        _tmpCamera.RenderWithShader(_drawSimpleShader, "");

        //copy the temporary RT to the final image
        Graphics.Blit(_tmpRenderTexture, destination, _postOutlineMaterial);
    }
}