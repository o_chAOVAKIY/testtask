﻿//This shader goes on the objects themselves. It just draws the object as white, and has the "Outline" tag.
Shader "DrawSimple"
{
    SubShader 
    {
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                return o;
            }
            
            fixed4 frag (v2f i) : SV_Target
            {
                return half4(1,1,1,1);
            }
 
            ENDCG
        }
    }
}