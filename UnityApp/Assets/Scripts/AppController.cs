﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AppController : MonoBehaviour
{
    [SerializeField] private PopUpItemView _popUpItemView = default;
    [SerializeField] private Button _closePopUp = default;
    [SerializeField] private InputController _inputController;

    [SerializeField] private string _serverUrl = default;

    private GameObject _assetsParent;
    private GameObject _selectedObject;
    private Config config;
    
    private readonly string jsonUrl = "config.json";

    private readonly Dictionary<string, RuntimePlatform> platforms = new Dictionary<string, RuntimePlatform>()
    {
        {"iOS", RuntimePlatform.IPhonePlayer},
        {"Android", RuntimePlatform.Android},
        {"WebGL", RuntimePlatform.WebGLPlayer},
        {"Windows", RuntimePlatform.WindowsPlayer}
    };

    void Start()
    {
        _assetsParent = new GameObject();
        _inputController.GameobjectToRotate = _assetsParent;
        _popUpItemView.ClosePopUp();
        GetJsonData();
    }

    private void OnEnable()
    {
        _closePopUp.onClick.AddListener(() => _popUpItemView.ClosePopUp());
    }

    private void OnDisable()
    {
        _closePopUp.onClick.RemoveListener(() => _popUpItemView.ClosePopUp());
    }

    private void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            var pointerPosition = Input.mousePosition;
            OnClick(pointerPosition);
        }
    }

    private void InstantiateAllObjectsFromAssetBundleBytes(byte[] bytes)
    {
        var assetBundle = AssetBundle.LoadFromMemory(bytes);
        var prefabs = assetBundle.LoadAllAssets<GameObject>();

        foreach (var pref in prefabs)
        {
            var go = Instantiate(pref);
            go.transform.parent = _assetsParent.transform;
            go.transform.localPosition = Vector3.zero;
            foreach (var child in go.GetComponentsInChildren<MeshRenderer>())
            {
                child.gameObject.AddComponent<MeshCollider>();
            }
        }
    }

    private void OnClick(Vector3 pointerPosition)
    {
        if (TryGetObjectByRaycast(pointerPosition, out var selectedObject))
        {
            if (_selectedObject != null)
            {
                _selectedObject.layer = 0;
            }
            _selectedObject = selectedObject;
            _selectedObject.layer = 8;
            if (IsSelectedObjectHot(out var hotObject))
            {
                SetPopUp(hotObject);
                LoadSpriteFromServer(hotObject.Image);
                Debug.Log("Found selected object");
            }
        }
    }

    private void LoadSpriteFromServer(string spriteUrl)
    {
        StartCoroutine(AssetsLoader.Instance.GetTexture(_serverUrl + spriteUrl, _popUpItemView.SetInfo));
    }

    private void SetPopUp(HotObjects hotObject)
    {
        _popUpItemView.SetInfo(hotObject.Title, hotObject.Description);
        _popUpItemView.gameObject.SetActive(true);
    }

    /// <summary>
    /// Get Object by pointer position
    /// </summary>
    /// <param name="pointerPosition"></param>
    /// <param name="selectedObject"></param>
    /// <returns></returns>
    private bool TryGetObjectByRaycast(Vector3 pointerPosition, out GameObject selectedObject)
    {
        var raycaster = Raycaster.Instance;
        var ray = raycaster.ScreenPointToRay(pointerPosition);
        if (raycaster.TryRaycastPhysics(ray, out var raycastHit))
        {
            selectedObject = raycastHit.collider.gameObject;

            Debug.Log(selectedObject.name);
            return true;
        }

        selectedObject = null;
        return false;
    }

    private void GetAssetBundle(string assetUrl)
    {
        StartCoroutine(AssetsLoader.Instance.LoadBytesData(_serverUrl + assetUrl, InstantiateAllObjectsFromAssetBundleBytes));
    }

    private void GetJsonData()
    {        
        StartCoroutine(AssetsLoader.Instance.LoadStringData(_serverUrl + jsonUrl, OnJsonReceived));
    }

    /// <summary>
    /// Invokes after json is received
    /// </summary>
    /// <param name="json"></param>
    private void OnJsonReceived(string json)
    {
        config = JsonUtility.FromJson<Config>(json);
        if (TryGetAssetUrlByPlatform(config.AssetBundles, out var assetUrl))
        {
            GetAssetBundle(assetUrl);
        }        
    }

    private bool TryGetAssetUrlByPlatform(AssetBundles[] assetBundles, out string url)
    {
        var appPlatform = Application.platform;
        if (appPlatform == RuntimePlatform.WindowsEditor) //To make it work inside Unity editor for tests
        {
            appPlatform = RuntimePlatform.WindowsPlayer;
        }

        foreach (var assetBundle in assetBundles)
        {
            if (appPlatform == platforms[assetBundle.Platform])
            {
                url = assetBundle.AssetBundle;
                return true;
            }
        }
        url = null;
        return false;
    }

    private bool IsSelectedObjectHot(out HotObjects hotObjects)
    {
        foreach (var hot in config.HotObjects)
        {
            if (_selectedObject.name == hot.Id)
            {
                hotObjects = hot;
                return true;
            }
        }

        hotObjects = null;
        return false;
    }
}
