﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycaster : Singleton<Raycaster>
{
    /// <summary>
    /// Used Unity's Physics raycaster
    /// </summary>
    /// <param name="ray"></param>
    /// <param name="raycastHit">Contains information about hit object</param>
    /// <returns>true if hit physic collider otherwise false </returns>
    public bool TryRaycastPhysics(Ray ray, out RaycastHit raycastHit)
    {
        return Physics.Raycast(ray, out raycastHit);
    }

    public Ray ScreenPointToRay(Vector3 screenPoint)
    {
        return Camera.main.ScreenPointToRay(screenPoint);
    }
}
