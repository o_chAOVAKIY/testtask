﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class AssetsLoader : Singleton<AssetsLoader>
{
    private AssetBundle _assetBundle;

    private void Start()
    {
        
    }

    /// <summary>
    /// Loads bytesarray from url
    /// </summary>
    /// <param name="callback">Callback with loaded bytesarray</param>
    /// <returns></returns>
    public IEnumerator LoadBytesData(string url, Action<byte[]> callback = null)
    {
        url = "https://appdev.virtualviewing.co.uk/developer_test/AssetBundles/Duplex_A_20110505!StandaloneWindows64!2020-06-24%2014-30-51.unity3d";
        if (Application.platform == RuntimePlatform.Android)
        {
            url = "https://appdev.virtualviewing.co.uk/developer_test/AssetBundles/Duplex_A_20110505!Android!2020-06-24 14-31-07.unity3d";
        }
        UnityWebRequest www = new UnityWebRequest(url);
        www.downloadHandler = new DownloadHandlerBuffer();
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            byte[] results = www.downloadHandler.data;
            callback?.Invoke(results);
        }
    }

    /// <summary>
    /// Loads string data from url
    /// </summary>
    /// <param name="callback">Callback with loaded string data</param>
    /// <returns></returns>
    public IEnumerator LoadStringData(string url, Action<string> callback = null)
    {
        UnityWebRequest www = new UnityWebRequest(url);
        www.downloadHandler = new DownloadHandlerBuffer();
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            // Show results as text
            Debug.Log(www.downloadHandler.text);

            callback?.Invoke(www.downloadHandler.text);
        }
    }

    public IEnumerator GetTexture(string url, Action<Texture> callback)
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();

        Texture myTexture = DownloadHandlerTexture.GetContent(www);
        callback?.Invoke(myTexture);
    }
}
