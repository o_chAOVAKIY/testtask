﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TextureExtension
{
    public static Sprite Tosprite(this Texture texture)
    {
        var rect = new Rect(0, 0, texture.width, texture.height);
        var sprt = Sprite.Create((Texture2D)texture, rect, new Vector2(0.5f, 0.5f));

        return sprt;
    }
}
