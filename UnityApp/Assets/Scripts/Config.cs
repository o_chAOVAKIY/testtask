﻿[System.Serializable]
public class Config
{
    public AssetBundles[] AssetBundles;
    public HotObjects[] HotObjects;
}

[System.Serializable]
public class AssetBundles
{
    public string Platform;
    public string AssetBundle;
}

[System.Serializable]
public class HotObjects
{
    public string Id;
    public string Title;
    public string Description;
    public string Image;
}
