﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public GameObject GameobjectToRotate;
    private const int MoveDelta = 5;

    private const float MouseZoomSpeed = 15.0f;
    private const float TouchZoomSpeed = 0.1f;
    private const float ZoomMinBound = -60f;
    private const float ZoomMaxBound = 20f;
    private const float MoveSpeed = 0.05f;
    private const float touchMovecofficient = 0.5f;
    private const float touchRotationCofficient = 0.001f;
    private const float rotationSpeed = 1000f;

    Vector2 firstFingerPosition;

    bool isDrag;
    bool isRotate;

    void Update()
    {
        if (Input.touchSupported)
        {
            // Pinch to zoom
            if (Input.touchCount == 2)
            {
                // get current touch positions
                Touch tZero = Input.GetTouch(0);
                Touch tOne = Input.GetTouch(1);
                // get touch position from the previous frame
                Vector2 tZeroPrevious = tZero.position - tZero.deltaPosition;
                Vector2 tOnePrevious = tOne.position - tOne.deltaPosition;

                float oldTouchDistance = Vector2.Distance(tZeroPrevious, tOnePrevious);
                float currentTouchDistance = Vector2.Distance(tZero.position, tOne.position);

                // get offset value
                float deltaDistance = currentTouchDistance - oldTouchDistance;
                Zoom(deltaDistance, TouchZoomSpeed);
            }

            //swipe using 4 fingers to awoid some android smartphones  triplefinger swipe issue(uses to create a screenshot on some devices)
            if (Input.touchCount == 4)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Moved)
                {
                    if (isDrag)
                    {
                        var delta = firstFingerPosition - Input.GetTouch(0).position;
                        Move(delta * touchMovecofficient);
                        firstFingerPosition = Input.GetTouch(0).position;
                    }
                    else
                    {
                        var delta = firstFingerPosition - Input.GetTouch(0).position;
                        if (Mathf.Abs(delta.x) > MoveDelta || Mathf.Abs(delta.y) > MoveDelta)
                        {
                            isDrag = true;
                            firstFingerPosition = Input.GetTouch(0).position;
                        }
                    }
                }
            }
            else if (isDrag && Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                isDrag = false;
            }

            //swipe finger to rotate
            if (Input.touchCount == 1)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Moved)
                {
                    if (isRotate)
                    {
                        var delta = firstFingerPosition - Input.GetTouch(0).position;
                        Rotate(delta);
                        firstFingerPosition = Input.GetTouch(0).position;
                    }
                    else
                    {
                        var delta = firstFingerPosition - Input.GetTouch(0).position;
                        if (Mathf.Abs(delta.x) > MoveDelta || Mathf.Abs(delta.y) > MoveDelta)
                        {
                            isRotate = true;
                            firstFingerPosition = Input.GetTouch(0).position;
                        }
                    }
                }
            }
            else if (isRotate && Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                isRotate = false;
            }
        }
        else
        {
            float scroll = Input.GetAxis("Mouse ScrollWheel");
            Zoom(scroll, MouseZoomSpeed);

            if (Input.GetMouseButton(1))
            {
                if (isDrag)
                {
                    var delta = firstFingerPosition - (Vector2)Input.mousePosition;
                    Move(delta * touchMovecofficient);
                    firstFingerPosition = (Vector2)Input.mousePosition;
                }
                else
                {
                    var delta = firstFingerPosition - (Vector2)Input.mousePosition;
                    if (Mathf.Abs(delta.x) > MoveDelta || Mathf.Abs(delta.y) > MoveDelta)
                    {
                        isDrag = true;
                        firstFingerPosition = Input.mousePosition;
                    }
                }                
            }
            else if (isDrag)
            {
                isDrag = false;
            }

            if (Input.GetMouseButton(0))
            {
                if (GameobjectToRotate != null)
                {
                    GameobjectToRotate.transform.Rotate(new Vector3(Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0) * Time.deltaTime * rotationSpeed, Space.World);
                }
            }
        }
    }

    void Zoom(float deltaMagnitudeDiff, float speed)
    {
        transform.position += transform.forward * deltaMagnitudeDiff * speed;
        // set min and max value of Clamp function upon your requirement
        var z = Mathf.Clamp(transform.position.z, ZoomMinBound, ZoomMaxBound);
        var newPos = transform.position;
        newPos.z = z;
        transform.position = newPos;
    }

    private void Move(Vector2 move)
    {
        Vector3 moveDelta = move * MoveSpeed;
        transform.position += moveDelta;
    }

    private void Rotate(Vector2 rotate)
    {
        GameobjectToRotate.transform.Rotate(new Vector2(rotate.y, rotate.x) * rotationSpeed * touchRotationCofficient, Space.World);
    }
}